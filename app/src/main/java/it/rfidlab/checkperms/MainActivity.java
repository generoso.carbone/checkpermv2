package it.rfidlab.checkperms;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private String dottedFile = "it.rfidlab.SP";
    private String regularFile = "SP";
    private String key = "KEY";
    private TextView regularTextView;
    private TextView dottedTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 666);
        } else{
            if(checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 667);
            }
        }

        regularTextView = findViewById(R.id.regular_value);
        dottedTextView = findViewById(R.id.dotted_value);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == 666 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            if(checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 667);
            }
        }

        if(requestCode == 667 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Toast.makeText(this, "Tutto ok", Toast.LENGTH_SHORT).show();
        }
    }

    ///
    /// onClick
    ///
    public void incrementDotted(View view) {
        SharedPreferences sp = getSharedPreferences(dottedFile, MODE_PRIVATE);

        // try to get the value inside the SharedPreferences;
        // if not found get 0
        int x = sp.getInt(key, 0);

        // increment x
        int t = x + 1;

        // write the updated value inside the shared preferences and get the return value
        // commit() returns true if the new values were successfully written to persistent storage
        // https://developer.android.com/reference/android/content/SharedPreferences.Editor.html#commit()
        boolean committed = sp.edit().putInt(key, t).commit();
        Log.e("MainActivity", x+ " " + String.valueOf(committed));
        if(committed)
            dottedTextView.setText(String.valueOf(t));
        else
            dottedTextView.setText(String.valueOf(x));
    }

    ///
    /// onClick
    ///
    public void incrementRegular(View view) {
        SharedPreferences sp = getSharedPreferences(regularFile, MODE_PRIVATE);

        // try to get the value inside the SharedPreferences;
        // if not found get 0
        int x = sp.getInt(key, 0);

        // increment x
        int t = x + 1;

        // write the updated value inside the shared preferences and get the return value
        // commit() returns true if the new values were successfully written to persistent storage
        // https://developer.android.com/reference/android/content/SharedPreferences.Editor.html#commit()
        boolean committed = sp.edit().putInt(key, t).commit();
        Log.e("MainActivity", x+ " " + String.valueOf(committed));
        if(committed)
            regularTextView.setText(String.valueOf(t));
        else
            regularTextView.setText(String.valueOf(x));
    }
}
